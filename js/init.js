let settings = $.get({url: "settings.dev.json", async: false});
if (!settings.responseJSON) {
    settings = $.get({url: "settings.json", async: false});
    if (!settings.responseJSON) {
        console.error('Failed to load settings.json', settings);
    }
}
const {responseJSON: global} = settings;

$(() => {
    $(document).on('submit', 'form', (e) => {
        const url = $(e.currentTarget).attr('uri');
        if (url) {
            e.preventDefault();

            $(`button[type='submit']`).prop('disabled', true);

            const method = $(e.currentTarget).attr('method');
            const data = new FormData($(e.currentTarget).get(0));
            const callback = $(e.currentTarget).attr('callback');

            $.ajax({
                url, method, data,
                contentType: false,
                processData: false,
            }).done((result) => {
                if (window[callback]) {
                    window[callback](result);
                }
            });
        }
    }).on('click', `form button[type='submit']`, (e) => {

    });
    $.ajaxSetup({
        api: true,
        async: true,
        beforeSend: async function (jqXHR, settings) {
            if (settings.api) {
                settings.url = global.apiUrl + settings.url;
            }
        },
        error: function ({responseJSON}) {
            try {
                const {status, code, response, error} = responseJSON || {};
                if (code >= 500) {
                    toastr.error('Ocurrió un error en la petición, por favor intente mas tarde.');
                    console.error(response.message, responseJSON);
                } else if (code >= 400) {
                    toastr.warning(response.message);
                    console.warn(response.message, responseJSON);
                }
            } catch (e) {
                toastr.error('Ocurrió un error en la petición, por favor intente mas tarde.');
                console.error(e, e);
            }
        },
        complete: function () {
            $(`button[type='submit']`).prop('disabled', false);
        }
    });
    if ($.fn.dataTable) {
        $.extend(true, $.fn.dataTable.defaults, {
            dom: 'Bfrtip',
            responsive: true,
            stateSave: true,
            order: [[0, 'asc']],
            buttons: [],
            ajax: {
                dataSrc: (name) => {
                    return ({status, code, data, error}) => data[name]
                }
            },
            columnDefs: (columns) => {
                columns.map((column, index) => {
                    column['targets'] = index;
                    return column;
                });
                return columns;
            },
            pageLength: 25,
            language: {
                search: "Buscar:",
                emptyTable: "No hay registros que consultar",
                lengthMenu: "Mostrar _MENU_ registros por pagina",
                info: "Mostrando pagina _PAGE_ de _PAGES_",
                loadingRecords: "Cargando...",
                paginate: {
                    first: "Primero",
                    last: "Ultimo",
                    next: "Siguiente",
                    previous: "Anterior"
                },
            },
        });
        global.dt = $.fn.dataTable.defaults;
    } else {
        console.info('Datatables not found');
    }
});

function checkValidity(e) {
    const $form = $(e.currentTarget).closest('form');
    if ($form.length) {
        return $(e.currentTarget).closest('form')[0].checkValidity();
    }
}
